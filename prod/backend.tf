terraform {
  backend "s3" {
    bucket         = "mad-angels-bucket"
    key            = "snow-tiger/prod/terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
  }
}