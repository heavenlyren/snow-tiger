# Snow Tiger



# Introduction

This repo is a playground to test the CI/CD pipeline of gitlab.
It will help understanding how to interact in a secure way with AWS as well as how to scale and manage an infrastructure using Puppet.


***

## Goal

- [ ] Deploy 2 EC2 instances through CI/CD pipeline in a secure way
- [ ] Configure a Puppet Master Server
- [ ] Configure a Web Server with Puppet

***

## Tools

- [ ] AWS
- [ ] Terraform
- [ ] Gitlab
- [ ] Puppet

