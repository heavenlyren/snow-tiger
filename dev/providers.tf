provider "aws" {
  region = "eu-west-1"
}

terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
    }
  }
}