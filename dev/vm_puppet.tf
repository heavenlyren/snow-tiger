resource "aws_instance" "puppet_vm" {
  ami                    = "ami-08ca3fed11864d6bb"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [aws_security_group.allow-ssh.id, aws_security_group.allow-internet-access.id,aws_security_group.allow-puppet-communication.id]
  user_data              = <<-EOF
                #!/bin/bash
                echo '127.0.0.1  puppet' | sudo tee -a /etc/hosts
                wget https://apt.puppet.com/puppet7-release-focal.deb
                sudo dpkg -i puppet7-release-focal.deb
                sudo apt-get update -y
                apt-get install puppetserver -y
                sudo sed -i 's/JAVA_ARGS=.*/JAVA_ARGS="-Xms256m -Xmx256m -Djruby.logger.class=com.puppetlabs.jruby_utils.jruby.Slf4jLogger"/' /etc/default/puppetserver
                sudo systemctl stop puppetserver
                sudo systemctl start puppetserver
                bash -l
                puppetserver -v
                EOF
  tags                   = {
    Name = "MA-EU1-TEST-PUPM"
    Type = "PUPPET-MASTER"
  }
}