resource "aws_security_group" "allow-8080" {
  name = "allow-8080"
  ingress {
    protocol    = "tcp"
    from_port   = 8080
    to_port     = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_security_group" "allow-puppet-communication" {
  name = "allow-puppet-communication"
  ingress {
    protocol    = "tcp"
    from_port   = 8140
    to_port     = 8140
    cidr_blocks = ["172.31.32.0/20"]
  }

  ingress {
    protocol    = "tcp"
    from_port = 8142
    to_port   = 8142
    cidr_blocks = ["172.31.32.0/20"]
  }

}

resource "aws_security_group" "allow-puppet-agent-communication" {
  name = "allow-puppet-agent-communication"
  egress {
    protocol    = "tcp"
    from_port   = 8140
    to_port     = 8140
    cidr_blocks = ["172.31.32.0/20"]
  }

  egress {
    protocol    = "tcp"
    from_port = 8142
    to_port   = 8142
    cidr_blocks = ["172.31.32.0/20"]
  }

}

resource "aws_security_group" "allow-internet-access" {
  name = "allow-internet-access"
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]

  }

}

resource "aws_security_group" "allow-ssh" {
  name = "allow-ssh"
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
}