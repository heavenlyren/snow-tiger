# Class Definition
class ntpconfig {
  # Installing NTP Package
  package {"ntp":
    ensure=> "present",
  }
  # Configuring NTP configuration file
  file {"/etc/ntp.conf":
    ensure=> "present",
    content=> "server ie.pool.ntp.org\n",
  }
  # Starting NTP services
  service {"ntp":
    ensure=> "running",
  }
}

# Class Declaration
include ntpconfig