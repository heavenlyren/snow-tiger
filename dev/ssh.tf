#resource "tls_private_key" "key" {
#  algorithm   = "RSA"
#  rsa_bits = "4096"
#}
#
resource "aws_key_pair" "keypair" {
  key_name   = "GITLAB_SSH_KEYPAIR"
  public_key = var.SSH_PUBLIC_KEY
}

#resource "local_file" "private_key" {
#  filename = "${aws_key_pair.keypair.key_name}.pem"
#  content  = tls_private_key.key.private_key_pem
#}
#
#resource "aws_s3_bucket_object" "object" {
#  bucket = "mad-angels-bucket"
#  key    = "snow-tiger/dev/${aws_key_pair.keypair.key_name}.pem"
#  acl    = "private"  # or can be "public-read"
#  source = local_file.private_key.filename
#
#}