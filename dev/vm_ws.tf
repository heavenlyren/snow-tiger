resource "aws_instance" "tfvm" {
  depends_on = [aws_instance.puppet_vm]
  ami                    = "ami-08ca3fed11864d6bb"
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.keypair.key_name
  vpc_security_group_ids = [
    aws_security_group.allow-8080.id, aws_security_group.allow-ssh.id, aws_security_group.allow-puppet-agent-communication.id,aws_security_group.allow-internet-access.id
  ]
  user_data              = <<-EOF
                #!/bin/bash
                wget https://apt.puppet.com/puppet7-release-focal.deb
                sudo dpkg -i puppet7-release-focal.deb
                sudo apt-get update -y
                sudo apt-get install puppet-agent -y
                sudo /opt/puppetlabs/bin/puppet resource service puppet ensure=running enable=true
                source /etc/profile.d/puppet-agent.sh
                EOF
  tags                   = {
    Name = "MA-EU1-TEST-WS1"
    Type = "WEB-SERVER"
  }
}