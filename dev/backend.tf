terraform {
  backend "s3" {
    bucket         = "mad-angels-bucket"
    key            = "snow-tiger/dev/terraform.tfstate"
    region         = "eu-west-1"
    encrypt        = true
  }
}