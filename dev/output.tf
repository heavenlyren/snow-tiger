output "ws_instance_ip" {
  value = aws_instance.tfvm.public_dns
}

output "puppet_instance_ip" {
  value = aws_instance.puppet_vm.public_dns
}

output "ws_hostname" {
  value = aws_instance.tfvm.private_dns
}

output "pupm_hostname" {
  value = aws_instance.puppet_vm.private_dns
}